#include "TerrariumSensors.h"

const uint8_t TemperatureSensor0[ 8 ] = { 40, 188, 219, 208, 4, 0, 0, 213 };
const uint8_t TemperatureSensor1[ 8 ] = { 40, 130, 108, 19, 5, 0, 0, 47 };

const float_t TerrariumSensors::InvalidValue = -127.0f;

TerrariumSensors::TerrariumSensors( const uint8_t pin, const uint8_t pin2 )
#ifdef TERRARIUM_ON_ARDUINO
	: readPin( pin ),
	oneWire( pin2 ),
	sensors( &oneWire )
#else
	: readPin( pin )
#endif
{
}

void TerrariumSensors::Setup()
{
#ifdef TERRARIUM_ON_ARDUINO
	sensors.begin();
	sensors.setResolution( TemperatureSensor0, 9 );
	sensors.setResolution( TemperatureSensor1, 9 );
#endif
}

SensorsStatus TerrariumSensors::Read( float_t& temperature, float_t& humidity, float_t& temperature0, float_t& temperature1 )
{
#ifndef TERRARIUM_ON_ARDUINO
	temperature = 1.0f;
	humidity = 1.0f;
	temperature0 = 1.0f;
	temperature1 = 1.0f;
	return ( status = SensorsStatus() );
#else

	requestReadings();
	temperature = getTempC();
	humidity = getHumi();

	sensors.requestTemperatures();
	temperature0 = sensors.getTempC( TemperatureSensor0 );
	temperature1 = sensors.getTempC( TemperatureSensor1 );

	if ( temperature0 == TerrariumSensors::InvalidValue )
		status.sensor0 = SensorsStatus::Error;
	if ( temperature1 == TerrariumSensors::InvalidValue )
		status.sensor0 = SensorsStatus::Error;

	return status;
#endif
}

void TerrariumSensors::requestReadings()
{
#ifdef TERRARIUM_ON_ARDUINO
#define PIN_TO_BASEREG(pin)             (&(digitalPinToPort(pin)->PIO_PER))
#else
	static volatile uint32_t dummy = 0;
#define PIN_TO_BASEREG(pin)             (&(dummy))
#endif
#define PIN_TO_BITMASK(pin)             (digitalPinToBitMask(pin))
#define IO_REG_TYPE uint32_t
#define DIRECT_READ(base, mask)         (((*((base)+15)) & (mask)) ? 1 : 0)

	uint8_t mask = 128;
	uint8_t idx = 0;

	IO_REG_TYPE bitMask = digitalPinToBitMask( readPin );
	volatile IO_REG_TYPE *reg = PIN_TO_BASEREG( readPin );

	for ( uint8_t i = 0; i < 5; i++ )
		readBuffer[ i ] = 0;

	storedTemperature = TerrariumSensors::InvalidValue;
	storedHumidity = TerrariumSensors::InvalidValue;

	pinMode( readPin, OUTPUT );
	digitalWrite( readPin, LOW );
	delay( 1 );
	digitalWrite( readPin, HIGH );
	delayMicroseconds( 40 );
	pinMode( readPin, INPUT );

	uint16_t loopCntLOW = TerrariumSensors::Timeout;
	while ( ( DIRECT_READ( reg, bitMask ) ) == LOW )
	{
		if ( --loopCntLOW == 0 )
		{
			status.multiSensor = SensorsStatus::TimeoutError;
			return;
		}
	}

	uint16_t loopCntHIGH = TerrariumSensors::Timeout;

	while ( ( DIRECT_READ( reg, bitMask ) ) != LOW )
	{
		if ( --loopCntHIGH == 0 )
		{
			status.multiSensor = SensorsStatus::TimeoutError;
			return;
		}
	}

	for ( uint8_t i = 40; i != 0; i-- )
	{
		loopCntLOW = TerrariumSensors::Timeout;

		while ( ( DIRECT_READ( reg, bitMask ) ) == LOW )
		{
			if ( --loopCntLOW == 0 )
			{
				status.multiSensor = SensorsStatus::TimeoutError;
				return;
			}
		}

		uint32_t t = micros();

		loopCntHIGH = TerrariumSensors::Timeout;

		while ( ( DIRECT_READ( reg, bitMask ) ) != LOW )
		{
			if ( --loopCntHIGH == 0 )
			{
				status.multiSensor = SensorsStatus::TimeoutError;
				return;
			}
		}

		if ( ( micros() - t ) > 40 )
		{
			readBuffer[ idx ] |= mask;
		}

		mask >>= 1;

		if ( mask == 0 )
		{
			mask = 128;
			idx++;
		}
	}

	pinMode( readPin, OUTPUT );
	digitalWrite( readPin, HIGH );

	float_t temp = ( word( readBuffer[ 2 ] & 0x7F, readBuffer[ 3 ] ) * 0.1f );
	float_t humi = ( word( readBuffer[ 0 ], readBuffer[ 1 ] ) * 0.1f );

	if ( temp < -40.0f || temp > 80.0f || humi < 0.0f || humi > 100.0f )
	{
		status.multiSensor = SensorsStatus::SuspiciousValues;
		return;
	}

	storedTemperature = temp;
	storedHumidity = humi;

	status.multiSensor = SensorsStatus::OK;
}