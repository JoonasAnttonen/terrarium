#ifndef TERRARIUM_DEFS_H
#define TERRARIUM_DEFS_H

//
// Uncomment the following line when compiling for Arduino.
//
//#define TERRARIUM_ON_ARDUINO

#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#ifdef TERRARIUM_ON_ARDUINO
#include <avr/pgmspace.h>
#include <SPI.h>
#include <Time.h>
//#include <Keypad.h>
#include <LiquidCrystal.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#endif

#define TERRARIUM_NUMBER_BUFFER_SIZE 16
typedef char char_t;
typedef float float_t;
typedef bool bool_t;

#ifndef TERRARIUM_ON_ARDUINO
#undef UNICODE
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <time.h>
#pragma comment (lib, "Ws2_32.lib")
struct FakeIPAddress { FakeIPAddress( int32_t, int32_t, int32_t, int32_t ) { } };
typedef FakeIPAddress IPAddress;
struct FakeEthernet
{
	void begin( const uint8_t[ 6 ] ) const { }
	void begin( const uint8_t[ 6 ], const uint8_t[ 4 ], const uint8_t[ 4 ], const uint8_t[ 4 ], const uint8_t[ 4 ] ) const { }
	IPAddress localIP() const { return IPAddress( 127, 0, 0, 1 ); }
};
static const FakeEthernet Ethernet;
struct FakeEthernetUDP
{
	void begin( int32_t ) const { }
	int32_t parsePacket() const { return 0; }
	void read( uint8_t*, int32_t ) const { }
	void beginPacket( const uint8_t[ 4 ], int32_t ) const { }
	void write( uint8_t*, int32_t ) const { }
	void endPacket() const { }
};
typedef FakeEthernetUDP EthernetUDP;
class FakeEthernetClient
{
	friend class FakeEthernetServer;
	SOCKET soc = INVALID_SOCKET;
public:

	FakeEthernetClient( SOCKET s = INVALID_SOCKET ) : soc( s ) { }
	~FakeEthernetClient() { }
	void stop() {
		if ( soc != INVALID_SOCKET ) {
			auto res = shutdown( soc, SD_SEND );
			if ( res == SOCKET_ERROR ) {
				printf( "shutdown failed with error: %d\n", WSAGetLastError() );
				closesocket( soc );
				return;
			}
			closesocket( soc );
			soc = INVALID_SOCKET;
		}
	}
	bool connected() {
		fd_set set{ 0 };
		FD_CLR( 0, &set );
		FD_SET( soc, &set );
		return ( select( 0, 0, &set, 0, 0 ) == 1 );
	}
	operator bool() { return soc != INVALID_SOCKET; }
	int32_t available() {
		fd_set set{ 0 };
		FD_CLR( 0, &set );
		FD_SET( soc, &set );
		timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		return ( select( 0, &set, 0, 0, &tv ) == 1 );
	}
	uint8_t peek() {
		char c;
		if ( recv( soc, &c, 1, MSG_PEEK ) != 1 ) { return -1; }
		return c;
	}
	int32_t read( uint8_t* buf ) { return read( buf, 1 ); }
	int32_t read( uint8_t* buf, int32_t len ) { return recv( soc, (char*)buf, len, 0 ); }
	int32_t write( uint8_t* buf, int32_t len ) { return send( soc, (char*)buf, len, 0 ); }
};
typedef FakeEthernetClient EthernetClient;
class FakeEthernetServer
{
	SOCKET soc = INVALID_SOCKET;
public:
	FakeEthernetServer( int32_t ) { }
	~FakeEthernetServer() { WSACleanup(); }
	void begin() {
		WSADATA wsaData;
		auto wRes = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
		if ( wRes != 0 ) {
			printf( "WSAStartup failed with error: %d\n", wRes );
			return;
		}
		struct addrinfo *result = NULL;
		struct addrinfo hints;
		ZeroMemory( &hints, sizeof( hints ) );
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;
		// Resolve the server address and port
		int iResult = getaddrinfo( NULL, "80", &hints, &result );
		if ( iResult != 0 ) {
			printf( "getaddrinfo failed with error: %d\n", iResult );
			return;
		}
		// Create a SOCKET for connecting to server
		soc = socket( result->ai_family, result->ai_socktype, result->ai_protocol );
		if ( soc == INVALID_SOCKET ) {
			printf( "socket failed with error: %ld\n", WSAGetLastError() );
			freeaddrinfo( result );
			return;
		}
		// Setup the TCP listening socket
		iResult = bind( soc, result->ai_addr, (int)result->ai_addrlen );
		if ( iResult == SOCKET_ERROR ) {
			printf( "bind failed with error: %d\n", WSAGetLastError() );
			freeaddrinfo( result );
			closesocket( soc );
			return;
		}
		freeaddrinfo( result );
	}
	EthernetClient available() {
		EthernetClient client;
		int32_t res = listen( soc, SOMAXCONN );
		if ( res == SOCKET_ERROR ) {
			printf( "listen failed with error: %d\n", WSAGetLastError() );
			closesocket( soc );
			return client;
		}
		fd_set readSet;
		FD_ZERO( &readSet );
		FD_SET( soc, &readSet );
		timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		// Wait until timeout or data received.
		res = select( 0, &readSet, 0, 0, &tv );
		if ( res == 1 ) {
			SOCKET s = accept( soc, 0, 0 );
			if ( s == INVALID_SOCKET ) {
				printf( "accept failed with error: %d\n", WSAGetLastError() );
				closesocket( s );
				return client;
			}
			client.soc = s;
		}
		else if ( res < 0 ) // Error. 
			res = -1;
		else // Timeout.
			res = 0;
		return client;
	}
};
typedef FakeEthernetServer EthernetServer;
struct FakeLiquidCrystal
{
	FakeLiquidCrystal( int32_t, int32_t, int32_t, int32_t, int32_t, int32_t ) { }
	void begin( int32_t, int32_t ) const { }
	void clear() const {
#if 0
		system( "cls" );
#endif
	}
	void print( const char* s ) const {
#if 0
		printf( "%s", s );
#endif
	}
	void print( int32_t i ) const {
#if 0
		printf( "%d", i );
#endif
	}
	void print( IPAddress ) const {
#if 0
		printf( "127.0.0.1" );
#endif
	}
	void cursor() const { }
	void noCursor() const { }
	void setCursor( int32_t c, int32_t r ) const {
#if 0
		COORD cor;
		cor.X = c;
		cor.Y = r; 
		SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), cor ); 
#endif
	}
};
typedef FakeLiquidCrystal LiquidCrystal;

struct FakeSerial
{
	void begin( int32_t ) const { }
	void print( const char* text ) const { printf( text ); }
	void print( const char c ) const { printf( "%c", c ); }
	void print( const unsigned char c ) const { printf( "%c", c ); }
	void print( const int32_t num ) const { printf( "%d", num ); }
	void print( const float_t num ) const { printf( "%f", num ); }
	void println( const char* text ) const { printf( "%s\n", text ); }
	void println( const int32_t num ) const { printf( "%d\n", num ); }
	void println( const float_t num ) const { printf( "%f\n", num ); }
	void println() const { printf( "\n" ); }
	void flush() const { }
};
static const FakeSerial Serial;

typedef uint8_t DeviceAddress[ 8 ];

//
// Here begins a set of defines that emulate the behaviour of the Arduino functions etc.
//

// Arduino <avr/pgmspace.h> macro to put stuff in the program flash memory. Does nothing on Windows.
#define PROGMEM
// Arduino #include <avr/pgmspace.h> pgm_read_byte() function. Pretty much a nop on Windows.
#define pgm_read_byte(a) (uint8_t)(*a)
// Arduino delay() function. Uses the Sleep function on Windows.
#define delay( i ) (Sleep( i ))

// Analog port names.
#define A5 0xA5
#define A4 0xA4
#define A3 0xA3
#define A2 0xA2
#define A1 0xA1
#define A0 0xA0

// Arduino <Time.h> hour() function. Works identically on Windows.
static uint32_t hour() {
	uint32_t t = (uint32_t)time( 0 );
	t = t + ( 2 * 60 * 60 );
	t /= 60;
	t /= 60;
	return t % 24;
}
// Arduino <Time.h> minute() function. Works identically on Windows.
static uint32_t minute() {
	uint32_t t = (uint32_t)time( 0 );
	t = t + ( 2 * 60 * 60 );
	t /= 60;
	return t % 60;
}
// Arduino <Time.h> second() function. Works identically on Windows.
static uint32_t second() {
	uint32_t t = (uint32_t)time( 0 );
	t = t + ( 2 * 60 * 60 );
	return t % 60;
}

template<typename T>
static T constrain( T x, const T a, const T b )
{
	if ( x <= a )
		return a;
	else if ( x >= b )
		return b;
	else return x;
}

//
// Here begins a set of defines that allow the windows build to work.
// BUT THEY WILL NOT DO THE RIGHT THING.
//

#define F_CPU 1

#define word( a,b ) (a | b)
#define digitalPinToBitMask( i ) (i)
#define digitalPinToPort( i ) (Pio())
#define micros() (uint32_t(15000000))
#define millis() (uint32_t(15000))

#define bitRead( x, n ) (0)
#define bitWrite( x, n, b )

#define timeNotSet 0
#define timeSet 1
#define timeNeedsSync 2
#define setTime(t)
#define setSyncProvider(p)
#define timeStatus() (timeSet)
#define now() (time(0))
#define year() (2014)
#define month() (10)
#define day() (13)

#define delayMicroseconds( i )

#define INPUT 0
#define OUTPUT 1
#define INPUT_PULLUP 2
#define pinMode( pin,mode )

#define HIGH 1
#define LOW 0
#define digitalRead(pin) 1
#define digitalWrite( pin, hilo )

#define analogRead(pin) pin

static volatile long unsigned int dummyRegister = 100;
#define portInputRegister(i)(&dummyRegister)
#endif

// Helper macro to define a string to the program memory and printing it to the serial connection.
#define LogEventToSerial( string )\
{\
static const uint8_t flashStr[] PROGMEM = { string };\
const uint8_t* str = flashStr;\
char_t c = 0;\
						while ( ( c = (char_t)pgm_read_byte( str++ ) ) )\
    Serial.print( c );\
Serial.println();}

#define TERRARIUM_HISTORY_ENTRIES 50

template<typename T>
static T constrainLoop( T x, const T a, const T b )
{
	if ( x < a )
		return b;
	else if ( x > b )
		return a;
	else return x;
}

struct TerrariumState
{
	bool_t Day;
	bool_t Raining;
	time_t LastRain;
	float_t Temperature;
	float_t Humidity;
	float_t Temperature0;
	float_t Temperature1;
};

struct TerrariumAlarms
{
	bool_t LowHumidity : 1;
	bool_t HighHumidity : 1;
	bool_t LowTemperature0 : 1;
	bool_t LowTemperature1 : 1;
	bool_t HighTemperature0 : 1;
	bool_t HighTemperature1 : 1;
};

struct TerrariumHistoryEntry
{
	time_t Timestamp;
	float_t Humidity;
	float_t Temperature0;
	float_t Temperature1;

	TerrariumHistoryEntry() : Timestamp( 0 ), Humidity( 0 ), Temperature0( 0 ), Temperature1( 0 ) { }
	operator bool_t() const { return ( Timestamp != 0 ); }
};

class TerrariumHistory
{
	TerrariumHistoryEntry History[ TERRARIUM_HISTORY_ENTRIES ];
	uint8_t ringPos;

public:

	uint8_t GetLength() const { return TERRARIUM_HISTORY_ENTRIES; }

	TerrariumHistory()
		: ringPos( 0 )
	{
		Clear();
	}

	const TerrariumHistoryEntry& Read( uint8_t i ) const
	{
		uint8_t actualPosition = ringPos + i;

		if ( actualPosition < TERRARIUM_HISTORY_ENTRIES )
			return History[ actualPosition ];
		else
			return History[ actualPosition - TERRARIUM_HISTORY_ENTRIES ];
	}

	void Write( const TerrariumState& state )
	{
		History[ ringPos ].Timestamp = now();
		History[ ringPos ].Humidity = state.Humidity;
		History[ ringPos ].Temperature0 = state.Temperature0;
		History[ ringPos ].Temperature1 = state.Temperature1;

		if ( ( ringPos + 1 ) == TERRARIUM_HISTORY_ENTRIES )
			ringPos = 0;
		else
			ringPos++;
	}

	void Clear()
	{
		memset( History, 0, sizeof( TerrariumHistoryEntry ) * TERRARIUM_HISTORY_ENTRIES );
	}
};

struct UpdateParams
{
	uint32_t History;
	uint32_t Sensors;

	UpdateParams( uint32_t history, uint32_t sensor ) : History( history ), Sensors( sensor ) { }
};

struct LightParams
{
	time_t StartOfDay;
	time_t EndOfDay;

	LightParams( time_t startOfDay, time_t endOfDay ) : StartOfDay( startOfDay ), EndOfDay( endOfDay ) { }
};

struct PumpParams
{
	uint32_t ActivatedWait;
	uint32_t ActiveFor;

	PumpParams( uint32_t activeFor, uint32_t activatedWait ) : ActiveFor( activeFor ), ActivatedWait( activatedWait ) { }
};

struct TemperatureParams
{
	float_t Low0;
	float_t Low1;
	float_t High0;
	float_t High1;

	TemperatureParams( float_t low0, float_t low1, float_t high0, float_t high1 ) : Low0( low0 ), Low1( low1 ), High0( high0 ), High1( high1 ) { }
};

struct HumidityParams
{
	float_t Low;
	float_t High;

	HumidityParams( float_t low, float_t high ) : Low( low ), High( high ) { }
};

//
// Represents the state of the Terrarium.
//
struct TerrariumParams
{
	TemperatureParams Temperature;
	HumidityParams Humidity;
	PumpParams Pump;
	LightParams Lights;
	UpdateParams Update;

	TerrariumParams( TemperatureParams tempParams, HumidityParams humidityParams, PumpParams pumpParams, LightParams lightParams, UpdateParams updateParams )
		: Temperature( tempParams ),
		  Humidity( humidityParams ),
		  Pump( pumpParams ),
		  Lights( lightParams ),
		  Update( updateParams )
	{
	}
};

#endif