#ifndef TERRARIUM_CLIMATE_SENSOR_H
#define TERRARIUM_CLIMATE_SENSOR_H

#include "TerrariumDefs.h"

#ifdef TERRARIUM_ON_ARDUINO
#define REQUIRESNEW false
#define REQUIRESALARMS false
#include <OneWire.h>
#include <DallasTemperature.h>
#endif

struct TerrariumSensors;

//
// Represents the status of the Humidity/Temperature sensor.
//
struct SensorsStatus
{
	// All is well.
	static const uint8_t OK = 0;
	// There was a checksum error when reading the sensor values.
	static const uint8_t ChecksumError = 1;
	// There was a timeout error when reading the sensor values.
	static const uint8_t TimeoutError = 2;
	// There was a timeout error when reading the sensor values.
	static const uint8_t SuspiciousValues = 3;

	static const uint8_t Error = 100;
	static const uint8_t Virtual = 4;

	// Automatic conversion operator.
	operator bool_t() const { return ( ( multiSensor == 0 ) && ( sensor0 == 0 ) && ( sensor1 == 0 ) ); }

	bool_t IsOK() const { return *this; }
	bool_t IsVirtual() const { return ( ( multiSensor == Virtual ) && ( sensor0 == Virtual ) && ( sensor1 == Virtual ) ); }

	SensorsStatus() : multiSensor( 0 ), sensor0( 0 ), sensor1( 0 ) { }
	SensorsStatus( uint8_t all ) : multiSensor( all ), sensor0( all ), sensor1( all ) { }
	SensorsStatus( uint8_t multi, uint8_t sensor0, uint8_t sensor1 ) : multiSensor( multi ), sensor0( sensor0 ), sensor1( sensor1 ) { }

private:

	friend struct TerrariumSensors;

	uint8_t multiSensor;
	uint8_t sensor0;
	uint8_t sensor1;
};

//
// Provides a way to control the Humidity/Temperature sensor of the Terrarium.
//
struct TerrariumSensors
{
	static const float_t InvalidValue;

	//
	// Create a new instance of TerrariumSensors.
	//
	TerrariumSensors( const uint8_t pin, const uint8_t pin2 );

	void Setup();

	//
	// Use this to read the sensor values.
	//
	SensorsStatus Read( float_t& temperature, float_t& humidity, float_t& temperature0, float_t& temperature1 );

private:

#ifdef TERRARIUM_ON_ARDUINO
	OneWire oneWire;
	DallasTemperature sensors;
#endif
	static const int32_t Timeout = F_CPU / 40000;

	uint8_t readPin;
	uint8_t readBuffer[ 5 ];

	float_t storedTemperature;
	float_t storedHumidity;

	SensorsStatus status;

	float_t getTempC() const { return storedTemperature; }
	float_t getHumi() const { return storedHumidity; };
	void requestReadings();
};

#endif