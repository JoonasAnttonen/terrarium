#include "TerrariumServer.h"

TerrariumServer::TerrariumServer()
	: server( 80 ),
	  enabled(false)
{
}

TerrariumServer::~TerrariumServer()
{
}

void TerrariumServer::Setup( const bool_t enable )
{
	enabled = enable;

	if ( enabled )
		server.begin();
}

void TerrariumServer::Loop()
{
	if ( !enabled )
		return;

	client = server.available();

	if ( client )
	{
		if ( client.available() )
		{
			OnClientConnected();

#ifndef TERRARIUM_ON_ARDUINO
			printf( "%s\n", "Client Connected!" );
#endif
			ParseRequest();

#ifndef TERRARIUM_ON_ARDUINO
			printf( "%s\n", "Client Disonnected!" );
#endif
			OnClientDisconnected();
		}

		client.stop();
	}
}

void TerrariumServer::ParseRequest()
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "ParseRequest Begin" );
#endif
	// Create the read buffer.
	TerrariumServerBuffer buffer;

	// Read the first header.
	uint8_t status = ReadHTTPHeader( buffer );

	if ( buffer.BeginsWithFlashStr( HTTP_METHOD_POST ) )
	{
		DiscardRestOfHeaderIfNeeded( status );

		bool haveCorrectContentType = false;

		while ( ( status = ReadHTTPHeader( buffer ) ) != ReadHeaderNoMore )
		{
			if ( !haveCorrectContentType )
			{
				if ( buffer.BeginsWithFlashStr( HTTP_HEADER_CONTENT_TYPE_KEY ) )
				{
					haveCorrectContentType = ( buffer.FindFlashStr( HTTP_HEADER_CONTENT_TYPE_VALUE, sizeof( HTTP_HEADER_CONTENT_TYPE_VALUE ) - 1, 0 ) >= 0 );

#ifndef TERRARIUM_ON_ARDUINO
					if ( haveCorrectContentType )
						printf( "%s\n", "Content-Type is correct!" );
					else
						printf( "%s\n", "Content-Type is NOT correct!" );
#endif
				}
			}

			DiscardRestOfHeaderIfNeeded( status );
		}

		if ( haveCorrectContentType )
		{
			ParseBody( buffer );
		}
		else
		{
			RespondWith400( API_JSON_400_BADCONTENTINFO );
		}
	}
	else
	{
		DiscardRestOfHeaderIfNeeded( status );
		DiscardHeaders();
		RespondWith501();
		return;
	}

#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "ParseRequest End" );
#endif
}

void TerrariumServer::WriteObjectHeader( const uint8_t* str )
{
	WriteFromFlash( TERRARIUM_JSON_QUOTE );
	WriteFromFlash( str );
	WriteFromFlash( TERRARIUM_JSON_QUOTE );
	WriteFromFlash( TERRARIUM_JSON_COLON );
}

void TerrariumServer::ParseBody( TerrariumServerBuffer& buffer )
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "ParseBody Begin" );
#endif

	// Ilmeisesti t�ss� kohtaa t�ytyy venaa et tulee dataa portille...
	// Ei edes kauheen montaa tuntia menny t�h�n liittyvien bugien korjauksessa. Vittu.
	delay( TERRARIUM_SERVER_READ_DELAY );

	ReadPOSTParam( buffer );

	if ( !buffer.BeginsWithFlashStr( API_METHODPARAM ) )
	{
#ifndef TERRARIUM_ON_ARDUINO
		printf( "%s\n", "Method param not found!" );
#endif
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	JsonWriter writer( *this );

	if ( buffer.FindFlashStr( API_ALL_GET, sizeof API_ALL_GET, sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();

		const TerrariumState& state = OnGetState();

		writer.BeginObject();
		writer.BeginObject( API_STATE_GET );
		writer.Write( API_TEMP_GET, (int32_t)state.Temperature0 );
		writer.Write( API_HUMI_GET, (int32_t)state.Humidity );
		writer.EndObject();
		writer.BeginObject( API_TEMP_GET );
		DoGetTemp( writer );
		writer.EndObject();
		writer.BeginObject( API_HUMI_GET );
		DoGetHumi( writer );
		writer.EndObject();
		writer.BeginObject( API_TIME_GET );
		DoGetTime( writer );
		writer.EndObject();
		writer.BeginObject( API_PUMP_GET );
		DoGetPump( writer );
		writer.EndObject();
		writer.BeginObject( API_DAY_GET );
		DoGetDay( writer );
		writer.EndObject();
		writer.BeginObject( API_UPDATE_GET );
		DoGetUpdateParams( writer );
		writer.EndObject();
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_TEMP_GET, sizeof API_TEMP_GET, sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();
		writer.BeginObject();
		DoGetTemp( writer );
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_TEMP_SET, sizeof API_TEMP_SET, sizeof API_METHODPARAM ) >= 0 ) {
		DoSetTempParams( buffer );
	}
	else if ( buffer.FindFlashStr( API_HUMI_GET, sizeof API_HUMI_GET, sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();
		writer.BeginObject();
		DoGetHumi( writer );
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_HUMI_SET, sizeof API_HUMI_SET, sizeof API_METHODPARAM ) >= 0 ) {
		DoSetHumiParams( buffer );
	}
	else if ( buffer.FindFlashStr( API_PUMP_GET, sizeof API_PUMP_GET, sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();
		writer.BeginObject();
		DoGetPump( writer );
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_PUMP_SET, sizeof API_PUMP_SET, sizeof API_METHODPARAM ) >= 0 ) {
		DoSetPumpParams( buffer );
	}
	else if ( buffer.FindFlashStr( API_DAY_GET, sizeof API_DAY_GET, sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();
		writer.BeginObject();
		DoGetDay( writer );
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_DAY_SET, sizeof API_DAY_SET, sizeof API_METHODPARAM ) >= 0 ) {
		DoSetDayParams( buffer );
	}

	else if ( buffer.FindFlashStr( API_UPDATE_GET, sizeof API_UPDATE_GET, sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();
		writer.BeginObject();
		DoGetUpdateParams( writer );
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_UPDATE_SET, sizeof API_UPDATE_SET, sizeof API_METHODPARAM ) >= 0 ) {
		DoSetUpdateParams( buffer );
	}

	else if ( buffer.FindFlashStr( API_TIME_GET, API_STRLEN( API_TIME_GET ), sizeof API_METHODPARAM ) >= 0 ) {
		WriteStandardHeaders();
		writer.BeginObject();
		DoGetTime( writer );
		writer.EndObject();
	}
	else if ( buffer.FindFlashStr( API_TIME_SET, API_STRLEN( API_TIME_SET ), sizeof API_METHODPARAM ) >= 0 ) {
		DoSetTime( buffer );
	}
	else
	{
#ifndef TERRARIUM_ON_ARDUINO
		printf( "%s\n", "Method param not recognized!" );
#endif
		RespondWith400( API_JSON_400_BADPARAMVALUES );
		return;
	}

#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "ParseBody End" );
#endif
}

void TerrariumServer::WriteStandardHeaders()
{
	WriteLineFromFlash( HTTP_HEADER_200 );
	WriteLineFromFlash( HTTP_HEADER_CONNECTION );
	WriteLineFromFlash( HTTP_HEADER_ACCESS_CONTROL );
	WriteLineFromFlash( HTTP_HEADER_CONTENT_TYPE_JSON );
	WriteLine();
}

//
// Attempts to find the position of the given parameter in the given buffer.
//
bool_t TerrariumServer::GetParameterLocation( TerrariumServerBuffer& buffer, const uint8_t* paramName, const size_t paramNameLen, int32_t& pos, int32_t& len )
{
	// Start by looking for the name of the parameter.
	int32_t parameterPos = buffer.FindFlashStr( paramName, paramNameLen, 0 );
	int32_t parameterLen = -1;

	// We did not find the parameter.
	if ( parameterPos < 0 )
		return false;

	// Move the parameterPos to the start of the actual parameter data. [paramName]+[=]
	parameterPos += paramNameLen + 1;

	// Try to find the end of the parameter by looking for the separator character.
	parameterLen = buffer.FindFlashStr( API_METHODPARAM_SEPARATOR, sizeof( API_METHODPARAM_SEPARATOR ), parameterPos ) - parameterPos;

	// If we did not find the separator, assume that this parameter runs all the way to the end of the buffer.
	if ( parameterLen < 0 )
		parameterLen = buffer.Length - parameterPos;

	pos = parameterPos;
	len = parameterLen;

	return true;
}

bool_t TerrariumServer::GetParameterAsNumber( TerrariumServerBuffer& buffer, const unsigned char* paramName, const size_t paramNameLen, uint32_t& number )
{
	int32_t parameterPos = -1;
	int32_t parameterLen = -1;

	// Read the first parameter.
	ReadPOSTParam( buffer );

	// Try to find the parameter value.
	if ( !GetParameterLocation( buffer, paramName, paramNameLen, parameterPos, parameterLen ) )
	{
		return false;
	}

	// Convert the parameter value to a number.
	char_t* numberBegin = &buffer.Data[ parameterPos ];
	char_t* numberEnd = &buffer.Data[ parameterPos + parameterLen ];
	number = strtoul( numberBegin, &numberEnd, 0 );
	return true;
}

void TerrariumServer::DoGetTemp( JsonWriter& writer )
{
	const TerrariumParams& params = OnGetParameters();
	const TerrariumHistory& history = OnGetHistory();

	writer.Write( API_TEMP_P1, (int32_t)params.Temperature.Low0 );
	writer.Write( API_TEMP_P2, (int32_t)params.Temperature.Low1 );
	writer.Write( API_TEMP_P3, (int32_t)params.Temperature.High0 );
	writer.Write( API_TEMP_P4, (int32_t)params.Temperature.High1 );
	writer.BeginArray( API_TEMP_P5 );

	for ( int32_t i = 0; i < history.GetLength(); i++ )
	{
		// Read an entry from the history.
		const TerrariumHistoryEntry& entry = history.Read( i );

		// Only send valid entries.
		if ( entry )
		{
			writer.BeginArray();
			writer.Write( (uint32_t)entry.Timestamp );
			writer.Write( (int32_t)entry.Temperature0 );
			writer.Write( (int32_t)entry.Temperature1 );
			writer.EndArray();
		}
	}

	writer.EndArray();
}

void TerrariumServer::DoSetTempParams( TerrariumServerBuffer& buffer )
{
	uint32_t low0 = 0;
	uint32_t low1 = 0;
	uint32_t high0 = 0;
	uint32_t high1 = 0;

	if ( !GetParameterAsNumber( buffer, API_TEMP_P1, API_STRLEN( API_TEMP_P1 ), low0 ) ||
		 !GetParameterAsNumber( buffer, API_TEMP_P2, API_STRLEN( API_TEMP_P2 ), low1 ) ||
		 !GetParameterAsNumber( buffer, API_TEMP_P3, API_STRLEN( API_TEMP_P3 ), high0 ) ||
		 !GetParameterAsNumber( buffer, API_TEMP_P4, API_STRLEN( API_TEMP_P4 ), high1 ) )
	{
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	if ( OnRequestSetTemperatureLimits( TemperatureParams( (float_t)low0, (float_t)low1, (float_t)high0, (float_t)high1 ) ) )
		RespondWith200();
	else
		RespondWith400( API_JSON_400_BADPARAMVALUES );
}

void TerrariumServer::DoGetHumi( JsonWriter& writer )
{
	const TerrariumParams& params = OnGetParameters();
	const TerrariumHistory& history = OnGetHistory();

	writer.Write( API_HUMI_P1, (int32_t)params.Humidity.Low );
	writer.Write( API_HUMI_P2, (int32_t)params.Humidity.High );
	writer.BeginArray( API_HUMI_P3 );

	for ( int32_t i = 0; i < history.GetLength(); i++ )
	{
		// Read an entry from the history.
		const TerrariumHistoryEntry& entry = history.Read( i );

		// Only send valid entries.
		if ( entry )
		{
			writer.BeginArray();
			writer.Write( (uint32_t)entry.Timestamp );
			writer.Write( (int32_t)entry.Humidity );
			writer.EndArray();
		}
	}

	writer.EndArray();
}

void TerrariumServer::DoSetHumiParams( TerrariumServerBuffer& buffer )
{
	uint32_t low = 0;
	uint32_t high = 0;

	if ( !GetParameterAsNumber( buffer, API_HUMI_P1, API_STRLEN( API_HUMI_P1 ), low ) ||
		 !GetParameterAsNumber( buffer, API_HUMI_P2, API_STRLEN( API_HUMI_P2 ), high ) )
	{
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	if ( OnRequestSetHumidityLimits( HumidityParams( (float_t)low, (float_t)high ) ) )
		RespondWith200();
	else
		RespondWith400( API_JSON_400_BADPARAMVALUES );
}

void TerrariumServer::DoGetPump( JsonWriter& writer )
{
	const TerrariumState& state = OnGetState();
	const TerrariumParams& params = OnGetParameters();

	writer.Write( API_PUMP_P3, (uint32_t)state.LastRain );
	writer.Write( API_PUMP_P1, (uint32_t)params.Pump.ActiveFor );
	writer.Write( API_PUMP_P2, (uint32_t)params.Pump.ActivatedWait );
}

void TerrariumServer::DoSetPumpParams( TerrariumServerBuffer& buffer )
{
	uint32_t activeFor = 0;
	uint32_t activatedWait = 0;

	if ( !GetParameterAsNumber( buffer, API_PUMP_P1, API_STRLEN( API_PUMP_P1 ), activeFor ) ||
		 !GetParameterAsNumber( buffer, API_PUMP_P2, API_STRLEN( API_PUMP_P2 ), activatedWait ) )
	{
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	if ( OnRequestSetPump( PumpParams( activeFor, activatedWait ) ) )
		RespondWith200();
	else
		RespondWith400( API_JSON_400_BADPARAMVALUES );
}

void TerrariumServer::DoGetDay( JsonWriter& writer )
{
	const TerrariumParams& params = OnGetParameters();
	const TerrariumHistory& history = OnGetHistory();

	writer.Write( API_DAY_P1, (int32_t)params.Lights.StartOfDay );
	writer.Write( API_DAY_P2, (int32_t)params.Lights.EndOfDay );
}

void TerrariumServer::DoSetDayParams( TerrariumServerBuffer& buffer )
{
	uint32_t startOfDay = 0;
	uint32_t endOfDay = 0;

	if ( !GetParameterAsNumber( buffer, API_DAY_P1, API_STRLEN( API_DAY_P1 ), startOfDay ) ||
		 !GetParameterAsNumber( buffer, API_DAY_P2, API_STRLEN( API_DAY_P2 ), endOfDay ) )
	{
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	if ( OnRequestSetDayLimits( LightParams( startOfDay, endOfDay ) ) )
		RespondWith200();
	else
		RespondWith400( API_JSON_400_BADPARAMVALUES );
}

void TerrariumServer::DoGetTime( JsonWriter& writer )
{
	writer.Write( API_TIME_P1, (uint32_t)now() );
}

void TerrariumServer::DoSetTime( TerrariumServerBuffer& buffer )
{
	uint32_t t = 0;

	if ( !GetParameterAsNumber( buffer, API_TIME_P1, API_STRLEN( API_TIME_P1 ), t ) )
	{
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	if ( OnRequestSetTime( t ) )
		RespondWith200();
	else
		RespondWith400( API_JSON_400_BADPARAMVALUES );
}

void TerrariumServer::DoGetUpdateParams( JsonWriter& writer )
{
	const TerrariumParams& params = OnGetParameters();

	writer.Write( API_UPDATE_P1, (int32_t)params.Update.History );
	writer.Write( API_UPDATE_P2, (int32_t)params.Update.Sensors );
}

void TerrariumServer::DoSetUpdateParams( TerrariumServerBuffer& buffer )
{
	uint32_t history = 0;
	uint32_t sensor = 0;

	if ( !GetParameterAsNumber( buffer, API_UPDATE_P1, API_STRLEN( API_UPDATE_P1 ), history ) ||
		 !GetParameterAsNumber( buffer, API_UPDATE_P2, API_STRLEN( API_UPDATE_P2 ), sensor ) )
	{
		RespondWith400( API_JSON_400_BADPARAMS );
		return;
	}

	if ( OnSetUpdateParams( UpdateParams( history, sensor ) ) )
		RespondWith200();
	else
		RespondWith400( API_JSON_400_BADPARAMVALUES );
}

void TerrariumServer::RespondWith200()
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "Responding With 200" );
#endif
	WriteLineFromFlash( HTTP_HEADER_200 );
	WriteLineFromFlash( HTTP_HEADER_CONNECTION );
	WriteLineFromFlash( HTTP_HEADER_ACCESS_CONTROL );
	WriteLine();
}

void TerrariumServer::RespondWith404()
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "Responding With 404" );
#endif
	WriteLineFromFlash( HTTP_HEADER_404 );
	WriteLineFromFlash( HTTP_HEADER_CONNECTION );
	WriteLineFromFlash( HTTP_HEADER_ACCESS_CONTROL );
	WriteLine();
}

void TerrariumServer::RespondWith500()
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "Responding With 500" );
#endif
	WriteLineFromFlash( HTTP_HEADER_500 );
	WriteLineFromFlash( HTTP_HEADER_CONNECTION );
	WriteLineFromFlash( HTTP_HEADER_ACCESS_CONTROL );
	WriteLine();
}

void TerrariumServer::RespondWith501()
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", "Responding With 501" );
#endif
	WriteLineFromFlash( HTTP_HEADER_501 );
	WriteLineFromFlash( HTTP_HEADER_CONNECTION );
	WriteLineFromFlash( HTTP_HEADER_ACCESS_CONTROL );
	WriteLine();
}

void TerrariumServer::RespondWith400( const unsigned char* json )
{
#ifndef TERRARIUM_ON_ARDUINO
	printf( "%s\n", json );
#endif
	WriteLineFromFlash( HTTP_HEADER_400 );
	WriteLineFromFlash( HTTP_HEADER_CONNECTION );
	WriteLineFromFlash( HTTP_HEADER_ACCESS_CONTROL );
	WriteLineFromFlash( HTTP_HEADER_CONTENT_TYPE_JSON );
	WriteLine();
	WriteFromFlash( json );
}

void TerrariumServer::ReadPOSTParam( TerrariumServerBuffer& buffer )
{
	buffer.Clear();

	for ( int32_t i = 0; i < TerrariumServerBuffer::SizeOf; i++ )
	{
		if ( client.available() )
		{
			client.read( (uint8_t*)&buffer.Data[ i ], 1 );
		}
		else
		{
			buffer.Data[ i ] = '\0';
			break;
		}

		buffer.Length++;

		if ( buffer.Data[ i ] == '&' )
		{
			buffer.Length--;
			buffer.Data[ i ] = '\0';
			break;
		}
	}

#ifndef TERRARIUM_ON_ARDUINO
	printf( "BODY: %s\n", buffer.Data );
#endif
}

uint8_t TerrariumServer::ReadHTTPHeader( TerrariumServerBuffer& buffer )
{
	int32_t i = 0;
	int32_t n = 0;
	char c = '\0';

	while ( ( i < TerrariumServerBuffer::SizeOf - 1 ) && ( c != '\n' ) )
	{
		if ( client.available() )
			n = client.read( (uint8_t*)&c, 1 );
		else
			n = 0;

		if ( n > 0 )
		{
			if ( c == '\r' )
			{
				c = (char)client.peek();
				n = (int32_t)c;

				if ( ( n > 0 ) && ( c == '\n' ) )
					client.read( (uint8_t*)&c, 1 );
				else
					c = '\n';
			}
			buffer.Data[ i ] = c;
			i++;
		}
		else
			c = '\n';
	}
	buffer.Data[ i ] = '\0';
	buffer.Length = i;

#ifndef TERRARIUM_ON_ARDUINO
	printf( "HEADER: %s", buffer.Data );
#endif

	if ( buffer.Length == 0 || ( buffer.Length > 0 && buffer.Data[ 0 ] == '\n' ) )
	{
		return ReadHeaderNoMore;
	}
	if ( buffer.Data[ buffer.Length - 1 ] != '\n' )
	{
		return ReadHeaderPartOnly;
	}

	return ReadOK;
}

void TerrariumServer::DiscardRestOfHeaderIfNeeded( uint8_t status )
{
	if ( status == ReadHeaderPartOnly )
	{
		DiscardHeader();
	}
}

void TerrariumServer::DiscardHeaders()
{
	while ( DiscardHeader() != ReadHeaderNoMore );
}

uint8_t TerrariumServer::DiscardHeader()
{
	int32_t len = DiscardLine();

	if ( len == 1 )
		return ReadHeaderNoMore;

	return ReadOK;
}

int32_t TerrariumServer::DiscardLine()
{
	int32_t i = 0;
	int32_t n = 0;
	char_t c = '\0';

	while ( c != '\n' )
	{
		if ( client.available() )
			n = client.read( (uint8_t*)&c, 1 );
		else
			n = 0;

		if ( n > 0 )
		{
			if ( c == '\r' )
			{
				c = (char_t)client.peek();
				n = (int32_t)c;

				if ( ( n > 0 ) && ( c == '\n' ) )
					client.read( (uint8_t*)&c, 1 );
				else
					c = '\n';
			}
			i++;
		}
		else
			c = '\n';
	}

	return i;
}

int32_t TerrariumServer::Write( int32_t number )
{
	char_t numberStr[ TERRARIUM_NUMBER_BUFFER_SIZE ];
	int32_t numberLen = sprintf( numberStr, "%ld", number );

	return Write( numberStr, numberLen );
}

int32_t TerrariumServer::Write( uint32_t number )
{
	char_t numberStr[ TERRARIUM_NUMBER_BUFFER_SIZE ];
	int32_t numberLen = sprintf( numberStr, "%lu", number );

	return Write( numberStr, numberLen );
}

int32_t TerrariumServer::Write( float_t number )
{
	char_t numberStr[ TERRARIUM_NUMBER_BUFFER_SIZE ];
	int32_t numberLen = sprintf( numberStr, "%f", number );

	return Write( numberStr, numberLen );
}

int32_t TerrariumServer::WriteLine()
{
	return Write( "\r\n", 2 );
}
int32_t TerrariumServer::Write( char_t* buf, int32_t len )
{
	int32_t written = 0;
	uint8_t buffer[ 32 ];
	int32_t bufferEnd = 0;
	const char_t* str = buf;

	for ( int32_t i = 0; i < len; i++ )
	{
		buffer[ bufferEnd++ ] = (uint8_t)*str++;

		if ( bufferEnd == 32 )
		{
			written += client.write( buffer, 32 );
			bufferEnd = 0;
		}
	}

	if ( bufferEnd > 0 )
		written += client.write( buffer, bufferEnd );

	return written;
}

int32_t TerrariumServer::WriteLineFromFlash( const uint8_t* str )
{
	int32_t written = WriteFromFlash( str );
	written += client.write( ( uint8_t* )"\r\n", 2 );
	return written;
}
int32_t TerrariumServer::WriteFromFlash( const uint8_t* str )
{
	uint8_t buffer[ 32 ];
	int32_t bufferEnd = 0;
	int32_t written = 0;

	while ( buffer[ bufferEnd++ ] = pgm_read_byte( str++ ) )
	{
		if ( bufferEnd == 32 )
		{
			written += client.write( buffer, 32 );
			bufferEnd = 0;
		}
	}

	if ( bufferEnd > 1 )
		written += client.write( buffer, bufferEnd - 1 );

	return written;
}