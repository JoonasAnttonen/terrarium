#ifndef TERRARIUM_KEYPAD_H
#define TERRARIUM_KEYPAD_H

#include "TerrariumDefs.h"

#define NO_KEY '\0'

enum KEYSTATE
{
	KEYSTATE_IDLE = 0,
	KEYSTATE_PRESSED = 1,
	KEYSTATE_RELEASED = 2,
	KEYSTATE_HELD = 3
};

#define TERRARIUM_KEYPAD_KEYS 12
#define TERRARIUM_KEYPAD_ROWS 4
#define TERRARIUM_KEYPAD_COLUMNS 3
#define TERRARIUM_KEYPAD_HOLD_TIME 500
#define TERRARIUM_KEYPAD_DEBOUNCE_TIME 10

#define LIST_MAX 10		// Max number of keys on the active list.
#define MAPSIZE 10		// MAPSIZE is the number of rows (times 16 columns)

class Key
{
public:
	char_t Char;
	int32_t Code;
	KEYSTATE State;
	bool_t stateChanged;

	bool_t IsPressed() const { return State == KEYSTATE_PRESSED; }
	bool_t IsHeld() const { return State == KEYSTATE_HELD; }
	bool_t IsReleased() const { return State == KEYSTATE_RELEASED; }
	bool_t IsIdle() const { return State == KEYSTATE_IDLE; }

	Key()
	{
		Char = NO_KEY;
		State = KEYSTATE_IDLE;
		stateChanged = false;
	}
	Key( char_t userKeyChar )
	{
		Char = userKeyChar;
		Code = -1;
		State = KEYSTATE_IDLE;
		stateChanged = false;
	}
	void key_update( char_t userKeyChar, KEYSTATE userState, bool_t userStatus )
	{
		Char = userKeyChar;
		State = userState;
		stateChanged = userStatus;
	}
};

class TerrariumKeypad
{
public:

	void( *OnKey )( const Key );

	TerrariumKeypad( const char_t keys[ TERRARIUM_KEYPAD_KEYS ], const uint8_t row[ TERRARIUM_KEYPAD_ROWS ], const uint8_t col[ TERRARIUM_KEYPAD_COLUMNS ] );

	bool_t Loop();

	bool_t IsPressed( char_t keyChar );
	int32_t findInList( char_t keyChar );
	int32_t findInList( int32_t keyCode );
	bool_t keyStateChanged();

private:

	void pin_mode( uint8_t pinNum, uint8_t mode ) { pinMode( pinNum, mode ); }
	void pin_write( uint8_t pinNum, bool_t level ) { digitalWrite( pinNum, level ); }
	int32_t pin_read( uint8_t pinNum ) { return digitalRead( pinNum ); }

	uint32_t bitMap[ MAPSIZE ];
	Key key[ LIST_MAX ];
	uint32_t holdTimer;

	uint32_t startTime;
	const char_t* keymap;
	const uint8_t* rowPins;
	const uint8_t* columnPins;

	void scanKeys();
	bool_t updateList();
	void nextKeyState( uint8_t n, bool_t button );
	void transitionTo( uint8_t n, KEYSTATE nextState );
};

#endif