#include "TerrariumKeypad.h"

TerrariumKeypad::TerrariumKeypad( const char_t keys[ TERRARIUM_KEYPAD_KEYS ], const uint8_t row[ TERRARIUM_KEYPAD_ROWS ], const uint8_t col[ TERRARIUM_KEYPAD_COLUMNS ] )
	: keymap( keys ),
	  rowPins( row ),
	  columnPins( col ),
	  startTime( 0 )
{
}

// Populate the key list.
bool_t TerrariumKeypad::Loop()
{
	bool_t keyActivity = false;

	// Limit how often the keypad is scanned. This makes the loop() run 10 times as fast.
	if ( ( millis() - startTime ) > TERRARIUM_KEYPAD_DEBOUNCE_TIME ) {
		scanKeys();
		keyActivity = updateList();
		startTime = millis();
	}

	return keyActivity;
}

// Private : Hardware scan
void TerrariumKeypad::scanKeys()
{
	// Re-intialize the row pins. Allows sharing these pins with other hardware.
	for ( uint8_t r = 0; r < TERRARIUM_KEYPAD_ROWS; r++ ) {
		pin_mode( rowPins[ r ], INPUT_PULLUP );
	}

	// bitMap stores ALL the keys that are being pressed.
	for ( uint8_t c = 0; c < TERRARIUM_KEYPAD_COLUMNS; c++ ) {
		pin_mode( columnPins[ c ], OUTPUT );
		pin_write( columnPins[ c ], LOW );	// Begin column pulse output.
		for ( uint8_t r = 0; r < TERRARIUM_KEYPAD_ROWS; r++ ) {
			bitWrite( bitMap[ r ], c, !pin_read( rowPins[ r ] ) );  // keypress is active low so invert to high.
		}
		// Set pin to high impedance input. Effectively ends column pulse.
		pin_write( columnPins[ c ], HIGH );
		pin_mode( columnPins[ c ], INPUT );
	}
}

// Manage the list without rearranging the keys. Returns true if any keys on the list changed state.
bool_t TerrariumKeypad::updateList()
{

	bool_t anyActivity = false;

	// Delete any IDLE keys
	for ( uint8_t i = 0; i < LIST_MAX; i++ ) {
		if ( key[ i ].State == KEYSTATE_IDLE ) {
			key[ i ].Char = NO_KEY;
			key[ i ].Code = -1;
			key[ i ].stateChanged = false;
		}
	}

	// Add new keys to empty slots in the key list.
	for ( uint8_t r = 0; r < TERRARIUM_KEYPAD_ROWS; r++ )
	{
		for ( uint8_t c = 0; c < TERRARIUM_KEYPAD_COLUMNS; c++ )
		{
			bool_t button = bitRead( bitMap[ r ], c );
			char_t keyChar = keymap[ r * TERRARIUM_KEYPAD_COLUMNS + c ];
			int32_t keyCode = r * TERRARIUM_KEYPAD_COLUMNS + c;
			int32_t idx = findInList( keyCode );
			// Key is already on the list so set its next state.
			if ( idx > -1 )
			{
				nextKeyState( idx, button );
			}
			// Key is NOT on the list so add it.
			if ( ( idx == -1 ) && button )
			{
				for ( uint8_t i = 0; i < LIST_MAX; i++ )
				{
					if ( key[ i ].Char == NO_KEY )
					{		// Find an empty slot or don't add key to list.
						key[ i ].Char = keyChar;
						key[ i ].Code = keyCode;
						key[ i ].State = KEYSTATE_IDLE;		// Keys NOT on the list have an initial state of IDLE.
						nextKeyState( i, button );
						break;	// Don't fill all the empty slots with the same key.
					}
				}
			}
		}
	}

	// Report if the user changed the state of any key.
	for ( uint8_t i = 0; i < LIST_MAX; i++ ) {
		if ( key[ i ].stateChanged ) anyActivity = true;
	}

	return anyActivity;
}

// Private
// This function is a state machine but is also used for debouncing the keys.
void TerrariumKeypad::nextKeyState( uint8_t idx, bool_t button )
{
	key[ idx ].stateChanged = false;

	switch ( key[ idx ].State ) {
		case KEYSTATE_IDLE:
			if ( button == HIGH ) {
				transitionTo( idx, KEYSTATE_PRESSED );
				holdTimer = millis();
			}		// Get ready for next HOLD state.
			break;
		case KEYSTATE_PRESSED:
			if ( ( millis() - holdTimer ) > TERRARIUM_KEYPAD_HOLD_TIME )	// Waiting for a key HOLD...
				transitionTo( idx, KEYSTATE_HELD );
			else if ( button == LOW )				// or for a key to be RELEASED.
				transitionTo( idx, KEYSTATE_RELEASED );
			break;
		case KEYSTATE_HELD:
			if ( button == LOW )
				transitionTo( idx, KEYSTATE_RELEASED );
			break;
		case KEYSTATE_RELEASED:
			transitionTo( idx, KEYSTATE_IDLE );
			break;
	}
}

// New in 2.1
bool_t TerrariumKeypad::IsPressed( char_t keyChar )
{
	for ( uint8_t i = 0; i < LIST_MAX; i++ ) 
	{
		if ( key[ i ].Char == keyChar ) 
		{
			if ( ( key[ i ].State == KEYSTATE_PRESSED ) && key[ i ].stateChanged )
				return true;
		}
	}
	return false;	// Not pressed.
}

// Search by character for a key in the list of active keys.
// Returns -1 if not found or the index into the list of active keys.
int32_t TerrariumKeypad::findInList( char_t keyChar )
{
	for ( uint8_t i = 0; i < LIST_MAX; i++ )
	{
		if ( key[ i ].Char == keyChar )
		{
			return i;
		}
	}
	return -1;
}

// Search by code for a key in the list of active keys.
// Returns -1 if not found or the index into the list of active keys.
int32_t TerrariumKeypad::findInList( int32_t keyCode )
{
	for ( uint8_t i = 0; i < LIST_MAX; i++ )
	{
		if ( key[ i ].Code == keyCode )
		{
			return i;
		}
	}
	return -1;
}

// The end user can test for any changes in state before deciding
// if any variables, etc. needs to be updated in their code.
bool_t TerrariumKeypad::keyStateChanged()
{
	return key[ 0 ].stateChanged;
}

void TerrariumKeypad::transitionTo( uint8_t idx, KEYSTATE nextState )
{
	key[ idx ].State = nextState;
	key[ idx ].stateChanged = true;

	if ( OnKey != NULL )
	{
		OnKey( key[ idx ] );
	}
}