#ifndef TERRARIUM_SERVER_H
#define TERRARIUM_SERVER_H

#include "TerrariumDefs.h"

#define TERRARIUM_SERVER_BUFFER_SIZE 64

#define TERRARIUM_SERVER_READ_DELAY 200

#define API_STRLEN(str) ((sizeof(str)) - 1 )

const uint8_t API_METHODPARAM[] PROGMEM = { "Method" };
const uint8_t API_METHODPARAM_SEPARATOR[] PROGMEM = { "&" };

const uint8_t API_STATE_GET[] PROGMEM = { "GetState" };

const uint8_t API_ALL_GET[] PROGMEM = { "GetAll" };

const uint8_t API_TEMP_GET[] PROGMEM = { "GetTemp" };
const uint8_t API_TEMP_SET[] PROGMEM = { "SetTempParams" };
const uint8_t API_TEMP_P1[] PROGMEM = { "Low0" };
const uint8_t API_TEMP_P2[] PROGMEM = { "Low1" };
const uint8_t API_TEMP_P3[] PROGMEM = { "High0" };
const uint8_t API_TEMP_P4[] PROGMEM = { "High1" };
const uint8_t API_TEMP_P5[] PROGMEM = { "Values" };

const uint8_t API_HUMI_GET[] PROGMEM = { "GetHumi" };
const uint8_t API_HUMI_SET[] PROGMEM = { "SetHumiParams" };
const uint8_t API_HUMI_P1[] PROGMEM = { "Low" };
const uint8_t API_HUMI_P2[] PROGMEM = { "High" };
const uint8_t API_HUMI_P3[] PROGMEM = { "Values" };

const uint8_t API_PUMP_GET[] PROGMEM = { "GetPump" };
const uint8_t API_PUMP_SET[] PROGMEM = { "SetPumpParams" };
const uint8_t API_PUMP_P1[] PROGMEM = { "ActiveFor" };
const uint8_t API_PUMP_P2[] PROGMEM = { "ActivatedWait" };
const uint8_t API_PUMP_P3[] PROGMEM = { "ActivatedLast" };

const uint8_t API_DAY_GET[] PROGMEM = { "GetDay" };
const uint8_t API_DAY_SET[] PROGMEM = { "SetDayParams" };
const uint8_t API_DAY_P1[] PROGMEM = { "StartOfDay" };
const uint8_t API_DAY_P2[] PROGMEM = { "EndOfDay" };

const uint8_t API_UPDATE_GET[] PROGMEM = { "GetUpdateParams" };
const uint8_t API_UPDATE_SET[] PROGMEM = { "SetUpdateParams" };
const uint8_t API_UPDATE_P1[] PROGMEM = { "History" };
const uint8_t API_UPDATE_P2[] PROGMEM = { "Sensor" };

const uint8_t API_TIME_GET[] PROGMEM = { "GetTime" };
const uint8_t API_TIME_SET[] PROGMEM = { "SetTime" };
const uint8_t API_TIME_P1[] PROGMEM = { "Time" };

const uint8_t HTTP_METHOD_POST[] PROGMEM = { "POST" };
const uint8_t HTTP_HEADER_200[] PROGMEM = { "HTTP/1.1 200 OK" };
const uint8_t HTTP_HEADER_400[] PROGMEM = { "HTTP/1.1 400 Bad Request" };
const uint8_t HTTP_HEADER_404[] PROGMEM = { "HTTP/1.1 404 Not Found" };
const uint8_t HTTP_HEADER_500[] PROGMEM = { "HTTP/1.1 500 Internal Server Error" };
const uint8_t HTTP_HEADER_501[] PROGMEM = { "HTTP/1.1 501 Not Implemented" };
const uint8_t HTTP_HEADER_CONNECTION[] PROGMEM = { "Connection: Close" };
const uint8_t HTTP_HEADER_ACCESS_CONTROL[] PROGMEM = { "Access-Control-Allow-Origin: *" };
const uint8_t HTTP_HEADER_CONTENT_TYPE_JSON[] PROGMEM = { "Content-Type: application/json" };
const uint8_t HTTP_HEADER_CONTENT_TYPE_KEY[] PROGMEM = { "Content-Type: " };
const uint8_t HTTP_HEADER_CONTENT_TYPE_VALUE[] PROGMEM = { "application/x-www-form-urlencoded" };

const uint8_t TERRARIUM_JSON_OPEN_OBJECT[] PROGMEM = { "{" };
const uint8_t TERRARIUM_JSON_OPEN_ARRAY[] PROGMEM = { "[" };
const uint8_t TERRARIUM_JSON_CLOSE_OBJECT[] PROGMEM = { "}" };
const uint8_t TERRARIUM_JSON_CLOSE_ARRAY[] PROGMEM = { "]" };
const uint8_t TERRARIUM_JSON_COLON[] PROGMEM = { ":" };
const uint8_t TERRARIUM_JSON_COMMA[] PROGMEM = { "," };
const uint8_t TERRARIUM_JSON_QUOTE[] PROGMEM = { "\"" };
const uint8_t TERRARIUM_JSON_INDENT[] PROGMEM = { "    " };

// Code 1, Bad or missing Content-Type
const uint8_t API_JSON_400_BADCONTENTINFO[] PROGMEM = { "{\"Error\":1}" };
// Code 2, Parameter order is wrong or missing parameters.
const uint8_t API_JSON_400_BADPARAMS[] PROGMEM = { "{\"Error\":2}" };
// Code 3, Invalid parameter values.
const uint8_t API_JSON_400_BADPARAMVALUES[] PROGMEM = { "{\"Error\":3}" };

//
// The Terrarium server read buffer.
//
struct TerrariumServerBuffer
{
	static const size_t SizeOf = TERRARIUM_SERVER_BUFFER_SIZE;

	char_t Data[ SizeOf ];
	uint32_t Length;

	TerrariumServerBuffer()
	{
		Length = sizeof( Data );
		Data[ Length ] = '\0';
	}

	const char_t& operator [] ( uint32_t i ) const
	{
		return Data[ i ];
	}

	void Clear()
	{
		memset( Data, 0, (size_t)SizeOf );
	}

	//
	// Test whether the buffer begins with the flash string.
	//
	bool_t BeginsWithFlashStr( const uint8_t* str )
	{
		uint8_t c = 0;
		uint32_t i = 0;

		// Loop until
		// the end of the buffer OR
		// the end of the flash string OR
		// the characters don't match.
		while ( ( i < this->Length ) && ( c = pgm_read_byte( str++ ) ) )
		{
			if ( ( ( uint8_t )this->Data[ i ] ) != c )
				return false;
			else
				i++;
		}

		return true;
	}

	//
	// Attempt to find the flash string in the buffer.
	//
	int32_t FindFlashStr( const uint8_t* str, const uint32_t strLen, const uint32_t offset )
	{
		int32_t str1Len = this->Length - offset;
		int32_t str2Len = strLen;
		int32_t findStop = false;
		int32_t findEnd = this->Length;

		if ( str1Len < str2Len )
			return -1;

		for ( int32_t i = offset; i < findEnd; i++ )
		{
			if ( ( ( uint8_t )this->Data[ i ] ) == pgm_read_byte( &str[ 0 ] ) )
			{
				findStop = true;

				for ( int32_t i2 = 1; i2 < str2Len - 1; i2++ )
				{
					if ( ( ( uint8_t )this->Data[ i + i2 ] ) != pgm_read_byte( &str[ i2 ] ) )
					{
						findStop = false;
						break;
					}
				}

				if ( findStop )
				{
					return i;
				}
			}
		}

		return -1;
	}
};

struct ServerBufferRef
{
	char_t* Data;
	uint32_t Length;

	ServerBufferRef( char_t* data, size_t length )
		: Data( data ),
		  Length( length )
	{
	}

	char_t operator [] ( uint32_t i ) const
	{
		if ( i < Length )
			return Data[ i ];
		else
			return 0;
	}
};

struct JsonWriter;

//
// The HTTP server of the Terrarium.
//
class TerrariumServer
{
	friend struct JsonWriter;
	static const uint8_t ReadOK = 0;
	static const uint8_t ReadHeaderPartOnly = 1;
	static const uint8_t ReadHeaderNoMore = 2;

	bool_t enabled;
	EthernetServer server;
	EthernetClient client;

public:

	void( *OnClientConnected )( );
	void( *OnClientDisconnected )( );
	const TerrariumState&( *OnGetState )( );
	const TerrariumParams&( *OnGetParameters )( );
	const TerrariumHistory&( *OnGetHistory )( );
	bool_t( *OnRequestSetTime )( time_t time );
	bool_t( *OnRequestSetPump )( const PumpParams& );
	bool_t( *OnRequestSetTemperatureLimits )( const TemperatureParams& );
	bool_t( *OnRequestSetHumidityLimits )( const HumidityParams& );
	bool_t( *OnRequestSetDayLimits )( const LightParams& );
	bool_t( *OnSetUpdateParams )( const UpdateParams& );

	//
	// GetInfo         Method=GetInfo -> {"Name":"xxx"}
	// SetInfo         Method=SetInfo&Name=xxx
	//
	// GetTemp         Method=GetTemp -> {"Min":xxx,"Max":xxx,"Values":[[xxx,yyy],[xxx,yyy],...]}
	// SetTempParams   Method=SetTempParams&Min=xxx&Max=xxx
	//
	// GetHumi         Method=GetHumi -> {"Min":xxx,"Max":xxx,"Values":[[xxx,yyy],[xxx,yyy],...]}
	// SetHumiParams   Method=SetHumiParams&Min=xxx&Max=xxx
	//
	// GetPump         Method=GetPump -> {"ActiveFor":xxx,"ActivatedWait":xxx}
	// SetPumpParams   Method=SetPumpParams&ActiveFor=xxx&ActivatedWait=xxx
	//
	// GetDay          Method=GetDay -> {"StartOfDay":xxx,"EndOfDay":xxx}
	// SetDayParams    Method=SetDayParams&StartOfDay=xxx&EndOfDay=xxx
	//
	// GetTime         Method=GetTime -> {"Time":xxx}
	// SetTime         Method=SetTime&Time=xxx
	//
	// GetUpdateParams Method=GetUpdateParams -> {"History":xxx,"Sensor":xxx}
	// SetUpdateParams Method=SetUpdateParams&History=xxx&Sensor=xxx
	//

	TerrariumServer();
	~TerrariumServer();

	// Initialize the server.
	// Call this from the setup function.
	void Setup( const bool_t enable );

	// The server checks for a waiting connection and if there is one, handles it.
	// Call this from the loop function.
	void Loop();

private:

	uint8_t ReadHTTPHeader( TerrariumServerBuffer& buffer );
	void ReadPOSTParam( TerrariumServerBuffer& buffer );

	int32_t DiscardLine();
	uint8_t DiscardHeader();
	void DiscardHeaders();
	void DiscardRestOfHeaderIfNeeded( uint8_t status );

	void ParseRequest();
	void ParseBody( TerrariumServerBuffer& buffer );

	bool_t GetParameterAsNumber( TerrariumServerBuffer& buffer, const uint8_t* paramName, const size_t paramNameLen, uint32_t& number );
	bool_t GetParameterLocation( TerrariumServerBuffer& buffer, const uint8_t* paramName, const size_t paramNameLen, int32_t& pos, int32_t& len );

	void DoGetTemp( JsonWriter& writer );
	void DoSetTempParams( TerrariumServerBuffer& buffer );
	void DoGetHumi( JsonWriter& writer );
	void DoSetHumiParams( TerrariumServerBuffer& buffer );
	void DoGetPump( JsonWriter& writer );
	void DoSetPumpParams( TerrariumServerBuffer& buffer );
	void DoGetDay( JsonWriter& writer );
	void DoSetDayParams( TerrariumServerBuffer& buffer );
	void DoGetUpdateParams( JsonWriter& writer );
	void DoSetUpdateParams( TerrariumServerBuffer& buffer );

	void DoGetTime( JsonWriter& writer );
	void DoSetTime( TerrariumServerBuffer& buffer );

	void WriteObjectHeader( const uint8_t* str );

	void WriteStandardHeaders();

	// Respond with HTTP 200 OK.
	void RespondWith200();
	// Respond with HTTP 400 Bad Request and additional error information in the body JSON.
	void RespondWith400( const uint8_t* json );
	// Respond with HTTP 404 Not Found.
	void RespondWith404();
	// Respond with HTTP 500 Internal Server Error.
	void RespondWith500();
	// Respond with HTTP 501 Not Implemented.
	void RespondWith501();

	int32_t Write( char_t c ){ return Write( &c, 1 ); }
	int32_t Write( uint8_t number ){ return Write( (uint32_t)number ); }
	int32_t Write( int32_t number );
	int32_t Write( uint32_t number );
	int32_t Write( float_t number );

	// Write a block of text to the client.
	int32_t Write( char_t* buf, int32_t len );
	// Write a line terminator to the client.
	int32_t WriteLine();

	// Write a block of text followed by a line terminator from the flash memory.
	int32_t WriteLineFromFlash( const uint8_t* str );
	// Write a block of text from the flash memory.
	int32_t WriteFromFlash( const uint8_t* str );
};

struct JsonLevel
{
	static const uint8_t TypeOfObject = 0;
	static const uint8_t TypeOfArray = 1;

	uint8_t Type : 1;
	uint8_t Nodes : 7;

	JsonLevel() : Type( 0 ), Nodes( 0 ) { }
	JsonLevel( uint8_t type ) : Type( type ), Nodes( 0 ) { }
	JsonLevel( uint8_t type, uint8_t nodes ) : Type( type ), Nodes( nodes ) { }
};

template<typename TypeOf, size_t SizeOf>
struct StaticStack
{
	static const size_t InvalidPointer = (size_t)-1;

	TypeOf stack[ SizeOf ];
	size_t stackPointer;

	StaticStack()
		: stackPointer( InvalidPointer )
	{
	}

	size_t Count() const
	{
		if ( stackPointer == InvalidPointer )
			return 0;
		else
			return ( stackPointer + 1 );
	}

	bool_t Push( TypeOf item )
	{
		if ( ( stackPointer + 1 ) >= SizeOf )
			return false;
		else if ( stackPointer == InvalidPointer )
			stackPointer = 0;
		else
			stackPointer = ( stackPointer + 1 );

		stack[ stackPointer ] = item;
		return true;
	}

	bool_t Peek( TypeOf*& item )
	{
		if ( stackPointer == InvalidPointer )
		{
			item = 0;
			return false;
		}

		item = &stack[ stackPointer ];
		return true;
	}
	bool_t Pop()
	{
		TypeOf* dummy;
		return Pop( dummy );
	}
	bool_t Pop( TypeOf*& item )
	{
		if ( stackPointer == InvalidPointer )
		{
			item = 0;
			return false;
		}

		item = &stack[ stackPointer ];
		stackPointer--;
		return true;
	}
};

struct JsonWriter
{
	StaticStack<JsonLevel, 64> levels;
	TerrariumServer& server;

	int32_t indendLevel;
	bool_t prettyPrint;

	JsonWriter( TerrariumServer& server ) : server( server ), prettyPrint( false ), indendLevel( 0 )
	{
	}

	void BeginObject( const uint8_t* name = 0 ) { Begin( JsonLevel::TypeOfObject, name ); }
	void BeginArray( const uint8_t* name = 0 ) { Begin( JsonLevel::TypeOfArray, name ); }
	void Begin( uint8_t type, const uint8_t* name )
	{
		if ( levels.Count() == 0 )
		{
			levels.Push( JsonLevel( type ) );
			indendLevel += 1;
		}
		else
		{
			WritePrefix();

			JsonLevel* level;
			levels.Peek( level );

			if ( level->Type == JsonLevel::TypeOfObject )
			{
				WriteString( name );
				WriteColon();
			}

			levels.Push( JsonLevel( type ) );
			indendLevel += 1;
		}

		if ( type == JsonLevel::TypeOfObject )
			server.WriteFromFlash( TERRARIUM_JSON_OPEN_OBJECT );
		else
			server.WriteFromFlash( TERRARIUM_JSON_OPEN_ARRAY );
	}

	void EndObject() { End(); }
	void EndArray() { End(); }
	void End()
	{
		if ( indendLevel > 0 )
			indendLevel -= 1;

		JsonLevel* level;
		levels.Peek( level );

		if ( level->Type == JsonLevel::TypeOfObject )
			WriteEnd( TERRARIUM_JSON_CLOSE_OBJECT );
		else
			WriteEnd( TERRARIUM_JSON_CLOSE_ARRAY );

		levels.Pop();
	}

	void WritePrefix()
	{
		JsonLevel* level;
		levels.Peek( level );

		if ( level->Nodes > 0 )
			WriteComma();

		level->Nodes++;

		WriteIndent();
	}

	void WriteEnd( const uint8_t* c )
	{
		if ( !prettyPrint )
		{
			server.WriteFromFlash( c );
		}
		else
		{
			server.WriteLine();
			WriteIndent();
			server.WriteFromFlash( c );
		}
	}

	void WriteComma()
	{
		if ( !prettyPrint )
			server.WriteFromFlash( TERRARIUM_JSON_COMMA );
		else
			server.WriteLineFromFlash( TERRARIUM_JSON_COMMA );
	}

	void WriteString( const uint8_t* str )
	{
		server.Write( '"' );
		server.WriteFromFlash( str );
		server.Write( '"' );
	}

	void WriteColon()
	{
		if ( !prettyPrint )
			server.WriteFromFlash( TERRARIUM_JSON_COLON );
		else
		{
			server.Write( ' ' );
			server.WriteFromFlash( TERRARIUM_JSON_COLON );
			server.Write( ' ' );
		}
	}

	void WriteIndent()
	{
		if ( prettyPrint )
			server.WriteFromFlash( TERRARIUM_JSON_INDENT );
	}

	void BeginNode( const uint8_t* name )
	{
		WritePrefix();

		JsonLevel* level;
		levels.Peek( level );

		if ( level->Type == JsonLevel::TypeOfObject )
		{
			WriteString( name );
			WriteColon();
		}
	}

	template<typename T>
	void Write( const T value ) { Write( 0, value ); }
	template<typename T>
	void Write( const uint8_t* name, const T value )
	{
		BeginNode( name );
		server.Write( value );
	}
};

#endif