#ifdef TERRARIUM_ON_ARDUINO
#include <Terrarium.h>
#else
#include "Terrarium.h"
#endif
#define SECONDS_PER_HOUR (60UL * 60UL)
#define SECONDS_PER_MINUTE (60UL)

// -----------------------------------------------------------
// Handy types for the main logic

enum SCREEN_STATE
{
	SCREEN_DEF,
	SCREEN_ADDRESS,
	SCREEN_OPTIONS,
};

enum SCREEN_SUB_STATE
{
	SCREEN_SUB_NONE,
	SCREEN_SUB_TEMPERATURE,
	SCREEN_SUB_HUMIDITY,
	SCREEN_SUB_DAYCYCLE,
	SCREEN_SUB_PUMP,
	SCREEN_SUB_CLOCK
};

enum KEY
{
	KEY_1 = '1',
	KEY_2 = '2',
	KEY_3 = '3',
	KEY_4 = '4',
	KEY_5 = '5',
	KEY_6 = '6',
	KEY_7 = '7',
	KEY_8 = '8',
	KEY_9 = '9',
	KEY_0 = '0',
	KEY_STAR = '*',
	KEY_HASH = '#'
};

// -----------------------------------------------------------
// Constants

//
// The MAC. Must be unique for devices on the same network, so keep that in mind if there are connectivity issues.
//
uint8_t mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

const uint32_t UpdateIntervalOfScreen = 200; // Milliseconds.

//
// The pump and the light are external hardware controlled with relays, 
// and there has been some confusion on what value is correct for what relay state, 
// so here they are defined for easy configuration.
//
const uint8_t ValueOfPumpOn = LOW;
const uint8_t ValueOfPumpOff = HIGH;
const uint8_t ValueOfLightOn = LOW;
const uint8_t ValueOfLightOff = HIGH;

//
// All hardware pin numbers are defined here.
// The keypad pin layout is especially confusing and one should consult the keypad schematic before changing ANYTHING.
//
const uint8_t PinOfKeypad = A0;
const uint8_t PinOfPump = A1;
const uint8_t PinOfLight = A2;
const uint8_t PinOfMultiSensor = 6;
const uint8_t PinOfTemperatureSensors = 4;
const uint8_t PinsOfKeymapRows[ TERRARIUM_KEYPAD_ROWS ] = { 47, 31, 33, 37 };
const uint8_t PinsOfKeymapColumns[ TERRARIUM_KEYPAD_COLUMNS ] = { 45, 49, 35 };

const char_t LayoutOfKeymap[ TERRARIUM_KEYPAD_KEYS ] =
{
	KEY_1, KEY_2, KEY_3,
	KEY_4, KEY_5, KEY_6,
	KEY_7, KEY_8, KEY_9,
	KEY_STAR, KEY_0, KEY_HASH
};

// -----------------------------------------------------------
// Terrarium components

LiquidCrystal Screen( A5, A4, 5, A3, 3, 2 );
TerrariumKeypad Keypad( LayoutOfKeymap, PinsOfKeymapRows, PinsOfKeymapColumns );
TerrariumSensors Sensors( PinOfMultiSensor, PinOfTemperatureSensors );
TerrariumState State;
TerrariumAlarms Alarms;
TerrariumServer Server;
TerrariumHistory History;
TerrariumParams Parameters(
	TemperatureParams( 20, 20, 30, 30 ),
	HumidityParams( 20, 40 ),
	PumpParams( 10, 60 ),
	LightParams( 8UL * SECONDS_PER_HOUR, 21UL * SECONDS_PER_HOUR ),
	UpdateParams( 2, 2 ) );

// -----------------------------------------------------------
// Basic variables

SCREEN_STATE screenState = SCREEN_DEF;
SCREEN_SUB_STATE screenSubState = SCREEN_SUB_NONE;
bool_t hasClient = false; // Whether or not the server has a client connected.
bool_t ethernetEnabled = false;
bool_t screenJustChanged = false;
time_t lastUpdateOfScreen = 0;
time_t lastUpdateOfHistory = 0;
time_t lastUpdateOfSensors = 0;

const int32_t menuMaxMain = 4;
const int32_t menuMaxTemperature = 3;
const int32_t menuMaxHumidity = 1;
const int32_t menuMaxDaycycle = 1;
const int32_t menuMaxPump = 1;
const int32_t menuMaxClock = 5;

int32_t menuSelection = 0;
int32_t menuSelectionMax = 4;
int32_t subMenuSelection = 0;

char_t numberBuffer[ TERRARIUM_NUMBER_BUFFER_SIZE ];
uint8_t numberBufferPos = 0;
bool_t gettingNumberInput = false;
bool_t gettingActualNumberInput = false;

// -----------------------------------------------------------
// Forward declarations/small implementations.

void OnKey( const Key key );
void Sunset();
void Sunrise();
void StopRaining();
void StartRaining();
void UpdateSensors();
void UpdateScreen();
void UpdateScreen( const SCREEN_STATE );
void UpdateSubScreen( const SCREEN_SUB_STATE, const int32_t );
bool_t IsItDaytime();
void OnClientConnected();
void OnClientDisconnected();
const TerrariumState& OnGetState() { return State; }
const TerrariumHistory& OnGetHistory() { return History; }
const TerrariumParams& OnGetParameters() { return Parameters; }
bool_t OnSetTime( time_t t );
bool_t OnSetPump( const PumpParams& );
bool_t OnSetDaytime( const LightParams& );
bool_t OnSetUpdateParams( const UpdateParams& );
bool_t OnSetHumidityParameters( const HumidityParams& );
bool_t OnSetTemperatureParameters( const TemperatureParams& );


// -----------------------------------------------------------
// Arduino setup

void setup()
{
	//
	// Setup serial comms and the LCD and the print the bootup message.
	//

	Serial.begin( 9600 );
	Screen.begin( 16, 2 );
	Screen.print( "PLEASE WAIT..." );

	//
	// Setup pump and light pins.
	//

	pinMode( PinOfPump, OUTPUT );
	pinMode( PinOfLight, OUTPUT );
	digitalWrite( PinOfPump, ValueOfPumpOff );
	digitalWrite( PinOfLight, ValueOfLightOff );

	//
	// Setup the keypad.
	//

	// Connect the event handler...
	Keypad.OnKey = &OnKey;
	// ...then run the loop function so we can catch boot-time input...
	Keypad.Loop();
	// ....lastly check for key 0 press. If it is pressed, disable the ethernet interface.
	ethernetEnabled = !Keypad.IsPressed( KEY_0 );

	//
	// Setup the ethernet interface.
	//

	if ( ethernetEnabled )
	{
		Ethernet.begin( mac );
	}

	//
	// Setup the API server
	//

	// Connect all the API server event handlers...
	Server.OnClientConnected = &OnClientConnected;
	Server.OnClientDisconnected = &OnClientDisconnected;
	Server.OnGetState = &OnGetState;
	Server.OnGetParameters = &OnGetParameters;
	Server.OnGetHistory = &OnGetHistory;
	Server.OnRequestSetTemperatureLimits = &OnSetTemperatureParameters;
	Server.OnRequestSetHumidityLimits = &OnSetHumidityParameters;
	Server.OnRequestSetDayLimits = &OnSetDaytime;
	Server.OnRequestSetTime = &OnSetTime;
	Server.OnRequestSetPump = &OnSetPump;
	Server.OnSetUpdateParams = &OnSetUpdateParams;
	// ...and then setup it.
	Server.Setup( ethernetEnabled );
}

// -----------------------------------------------------------
// Arduino loop

void loop()
{
	//
	// Update the sensors if needed.
	//
	if ( ( now() - lastUpdateOfSensors ) > Parameters.Update.Sensors )
	{
		UpdateSensors();
		lastUpdateOfSensors = now();
	}

	//
	// Update the screen if needed.
	//
	if ( ( millis() - lastUpdateOfScreen ) > UpdateIntervalOfScreen )
	{
		UpdateScreen();
		lastUpdateOfScreen = millis();
	}

	//
	// Make sure the light is in the correct state.
	//
	if ( IsItDaytime() )
	{
		if ( !State.Day )
		{
			Sunrise();
		}
	}
	else
	{
		if ( State.Day )
		{
			Sunset();
		}
	}

	//
	// Make sure the rainmaker (heh) is in the correct state.
	//
	if ( Alarms.LowHumidity )
	{
		if ( State.Raining )
		{
			if ( ( now() - State.LastRain ) > Parameters.Pump.ActiveFor )
			{
				StopRaining();
			}
		}
		else if ( ( now() - State.LastRain ) > Parameters.Pump.ActivatedWait )
		{
			StartRaining();
		}
	}
	else if ( State.Raining )
	{
		StopRaining();
	}

	//
	// Handle clients and input.
	//
	Server.Loop();
	Keypad.Loop();
}

void UpdateSubScreen( const SCREEN_SUB_STATE state, const int32_t menuMax )
{
	// Clear the screen...
	Screen.clear();
	// ...update the sub screen state...
	screenSubState = state;

	subMenuSelection = 0;
	menuSelectionMax = menuMax;

	// ...and render.
	UpdateScreen();
}

void UpdateScreen( const SCREEN_STATE state )
{
	// Clear the screen...
	Screen.clear();
	// ...update the screen state...
	screenState = state;
	// ...and render.
	UpdateScreen();
}

void UpdateScreen()
{
	// For text buffering.
	char_t buf[ 16 ];
	int32_t numberToDisplay = 0;

	//
	// Render the screen based on the screen state. 
	//
	switch ( screenState )
	{
		case SCREEN_DEF:
			Screen.setCursor( 0, 0 );
			sprintf( buf, "%.2d.%.2d.%d", day(), month(), year() );
			Screen.print( buf );

			Screen.setCursor( 10, 0 );
			if ( hasClient ) Screen.print( '@' );
			else Screen.print( ' ' );

			Screen.setCursor( 11, 0 );
			sprintf( buf, "%.2d:%.2d", hour(), minute() );
			Screen.print( buf );

			Screen.setCursor( 0, 1 );
			sprintf( buf, "%3.0f", State.Temperature0 );
			Screen.print( buf );

			if ( Alarms.LowTemperature0 ) Screen.print( '-' );
			else if ( Alarms.HighTemperature0 ) Screen.print( '+' );
			else Screen.print( 'C' );

			Screen.setCursor( 5, 1 );
			sprintf( buf, "%3.0f", State.Temperature1 );
			Screen.print( buf );

			if ( Alarms.LowTemperature1 ) Screen.print( '-' );
			else if ( Alarms.HighTemperature1 ) Screen.print( '+' );
			else Screen.print( 'C' );

			Screen.setCursor( 11, 1 );
			sprintf( buf, "%3.0f", State.Humidity );
			Screen.print( buf );

			if ( Alarms.LowHumidity ) Screen.print( '-' );
			else if ( Alarms.HighHumidity ) Screen.print( '+' );
			else Screen.print( '%' );

			break;
		case SCREEN_ADDRESS:
			Screen.setCursor( 0, 0 );
			if ( ethernetEnabled )
			{
				Screen.print( "IP ADDRESS" );
				Screen.setCursor( 0, 1 );
				Screen.print( Ethernet.localIP() );
			}
			else
			{
				Screen.print( "ETHERNET" );
				Screen.setCursor( 0, 1 );
				Screen.print( "DISABLED" );
			}
			break;
		case SCREEN_OPTIONS:
			Screen.setCursor( 0, 0 );

			if ( screenSubState == SCREEN_SUB_NONE )
			{
				Screen.print( "ADJUST SETTINGS" );
				Screen.setCursor( 0, 1 );
				switch ( menuSelection )
				{
					case 0: Screen.print( "> TEMPERATURE" ); break;
					case 1: Screen.print( "> HUMIDITY   " ); break;
					case 2: Screen.print( "> DAYCYCLE   " ); break;
					case 3: Screen.print( "> PUMP       " ); break;
					case 4: Screen.print( "> CLOCK      " ); break;
				}
			}
			else
			{
				switch ( screenSubState )
				{
					case SCREEN_SUB_TEMPERATURE:
						if ( !gettingNumberInput )
						{
							Screen.print( "TEMPERATURE     " );
							Screen.setCursor( 0, 1 );
							Screen.print( "> " );
						}
						switch ( subMenuSelection )
						{
							case 0: Screen.print( "COOL LOW      " ); numberToDisplay = (int32_t)Parameters.Temperature.Low0; break;
							case 1: Screen.print( "COOL HIGH     " ); numberToDisplay = (int32_t)Parameters.Temperature.High0; break;
							case 2: Screen.print( "WARM LOW      " ); numberToDisplay = (int32_t)Parameters.Temperature.Low1; break;
							case 3: Screen.print( "WARM HIGH     " ); numberToDisplay = (int32_t)Parameters.Temperature.High1; break;
						}
						break;
					case SCREEN_SUB_HUMIDITY:
						if ( !gettingNumberInput )
						{
							Screen.print( "HUMIDITY   " );
							Screen.setCursor( 0, 1 );
							Screen.print( "> " );
						}
						switch ( subMenuSelection )
						{
							case 0: Screen.print( "LOW        " ); numberToDisplay = (int32_t)Parameters.Humidity.Low; break;
							case 1: Screen.print( "HIGH       " ); numberToDisplay = (int32_t)Parameters.Humidity.High; break;
						}
						break;
					case SCREEN_SUB_DAYCYCLE:
						if ( !gettingNumberInput )
						{
							Screen.print( "DAYCYCLE   " );
							Screen.setCursor( 0, 1 );
							Screen.print( "> " );
						}
						switch ( subMenuSelection )
						{
							case 0: Screen.print( "DAY BEGIN     " ); numberToDisplay = (int32_t)Parameters.Lights.StartOfDay; break;
							case 1: Screen.print( "DAY END       " ); numberToDisplay = (int32_t)Parameters.Lights.EndOfDay;  break;
						}
						break;
					case SCREEN_SUB_PUMP:
						if ( !gettingNumberInput )
						{
							Screen.print( "PUMP       " );
							Screen.setCursor( 0, 1 );
							Screen.print( "> " );
						}
						switch ( subMenuSelection )
						{
							case 0: Screen.print( "ACTIVE FOR    " ); numberToDisplay = (int32_t)Parameters.Pump.ActiveFor; break;
							case 1: Screen.print( "ACTIVATED WAIT" ); numberToDisplay = (int32_t)Parameters.Pump.ActivatedWait; break;
						}
						break;
					case SCREEN_SUB_CLOCK:
						if ( !gettingNumberInput )
						{
							Screen.print( "CLOCK      " );
							Screen.setCursor( 0, 1 );
							Screen.print( "> " );
						}
						switch ( subMenuSelection )
						{
							case 0: Screen.print( "YEAR  " ); break;
							case 1: Screen.print( "MONTH " ); break;
							case 3: Screen.print( "DAY   " ); break;
							case 4: Screen.print( "HOUR  " ); break;
							case 5: Screen.print( "MINUTE" ); break;
						}
						break;
				}

				if ( gettingNumberInput )
				{
					Screen.setCursor( 0, 1 );
					Screen.print( "> " );
					if ( gettingActualNumberInput ) Screen.print( numberBuffer );
					else Screen.print( numberToDisplay );
				}
			}
			break;
		default:
			break;
	}
}

void AppendToNumber( char_t c )
{
	if ( numberBufferPos < TERRARIUM_NUMBER_BUFFER_SIZE - 1 )
	{
		numberBuffer[ numberBufferPos++ ] = c;
		numberBuffer[ numberBufferPos ] = '\0';
	}
}

void OnKey( Key key )
{
	switch ( key.Char )
	{
		case KEY_0:
			if ( key.IsPressed() )
			{
				if ( screenState == SCREEN_DEF )
				{
					UpdateScreen( SCREEN_ADDRESS );
				}
				else if ( gettingActualNumberInput )
				{
					AppendToNumber( KEY_0 );
				}
			}
			else if ( key.IsReleased() )
			{
				if ( screenState == SCREEN_ADDRESS )
				{
					UpdateScreen( SCREEN_DEF );
				}
			}
			break;
		case KEY_1: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_1 ); break;
		case KEY_2: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_2 ); break;
		case KEY_3: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_3 ); break;
		case KEY_4: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_4 ); break;
		case KEY_5: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_5 ); break;
		case KEY_6: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_6 ); break;
		case KEY_7:
			if ( key.IsPressed() )
			{
				if ( screenState == SCREEN_OPTIONS )
				{
					if ( screenSubState == SCREEN_SUB_NONE )
					{
						menuSelection = constrainLoop( menuSelection - (int32_t)1, (int32_t)0, menuSelectionMax );
					}
					else if ( gettingActualNumberInput )
					{
						AppendToNumber( KEY_7 );
					}
					else
					{
						subMenuSelection = constrainLoop( subMenuSelection - (int32_t)1, (int32_t)0, menuSelectionMax );
						UpdateScreen( screenState );
					}
				}
			}
			break;
		case KEY_8: if ( key.IsPressed() && gettingActualNumberInput ) AppendToNumber( KEY_8 ); break;
		case KEY_9:
			if ( key.IsPressed() )
			{
				if ( screenState == SCREEN_OPTIONS )
				{
					if ( screenSubState == SCREEN_SUB_NONE )
					{
						menuSelection = constrainLoop( menuSelection + (int32_t)1, (int32_t)0, menuSelectionMax );
					}
					else if ( gettingActualNumberInput )
					{
						AppendToNumber( KEY_9 );
					}
					else
					{
						subMenuSelection = constrainLoop( subMenuSelection + (int32_t)1, (int32_t)0, menuSelectionMax );
						UpdateScreen( screenState );
					}
				}
			}
			break;
		case KEY_HASH:
			if ( key.IsPressed() )
			{
				if ( screenState == SCREEN_DEF )
				{
					menuSelectionMax = menuMaxMain;
					UpdateScreen( SCREEN_OPTIONS );
				}
				else if ( screenState == SCREEN_OPTIONS )
				{
					if ( screenSubState == SCREEN_SUB_NONE )
					{
						switch ( menuSelection )
						{
							case 0: UpdateSubScreen( SCREEN_SUB_TEMPERATURE, menuMaxTemperature ); break;
							case 1: UpdateSubScreen( SCREEN_SUB_HUMIDITY, menuMaxHumidity ); break;
							case 2: UpdateSubScreen( SCREEN_SUB_DAYCYCLE, menuMaxDaycycle ); break;
							case 3: UpdateSubScreen( SCREEN_SUB_PUMP, menuMaxPump ); break;
							case 4: UpdateSubScreen( SCREEN_SUB_CLOCK, menuMaxClock ); break;
						}
					}
					else if ( gettingNumberInput )
					{
						if ( gettingActualNumberInput )
						{
							switch ( screenSubState )
							{
								case SCREEN_SUB_TEMPERATURE:
									switch ( subMenuSelection )
									{
										case 0: OnSetTemperatureParameters( TemperatureParams( (float_t)strtoul( &numberBuffer[ 0 ], 0, 0 ), Parameters.Temperature.Low1, Parameters.Temperature.High0, Parameters.Temperature.High1 ) ); break;
										case 1: OnSetTemperatureParameters( TemperatureParams( Parameters.Temperature.Low0, Parameters.Temperature.Low1, (float_t)strtoul( &numberBuffer[ 0 ], 0, 0 ), Parameters.Temperature.High1 ) ); break;
										case 2: OnSetTemperatureParameters( TemperatureParams( Parameters.Temperature.Low0, (float_t)strtoul( &numberBuffer[ 0 ], 0, 0 ), Parameters.Temperature.High0, Parameters.Temperature.High1 ) ); break;
										case 3: OnSetTemperatureParameters( TemperatureParams( Parameters.Temperature.Low0, Parameters.Temperature.Low1, Parameters.Temperature.High0, (float_t)strtoul( &numberBuffer[ 0 ], 0, 0 ) ) ); break;
									}
									break;
								case SCREEN_SUB_HUMIDITY:
									switch ( subMenuSelection )
									{
										case 0: OnSetHumidityParameters( HumidityParams( (float_t)strtoul( &numberBuffer[ 0 ], 0, 0 ), Parameters.Humidity.High ) ); break;
										case 1: OnSetHumidityParameters( HumidityParams( Parameters.Humidity.Low, (float_t)strtoul( &numberBuffer[ 0 ], 0, 0 ) ) ); break;
									}
									break;
								case SCREEN_SUB_DAYCYCLE:
									break;
								case SCREEN_SUB_PUMP:
									switch ( subMenuSelection )
									{
										case 0: OnSetPump( PumpParams( strtoul( &numberBuffer[ 0 ], 0, 0 ), Parameters.Pump.ActivatedWait ) ); break;
										case 1: OnSetPump( PumpParams( Parameters.Pump.ActiveFor, strtoul( &numberBuffer[ 0 ], 0, 0 ) ) ); break;
									}
									break;
									break;
								case SCREEN_SUB_CLOCK:
									break;
							}

							gettingActualNumberInput = false;
							Screen.noCursor();
							gettingNumberInput = false;
							numberBuffer[ 0 ] = '\0';
							numberBufferPos = 0;
						}
						else
						{
							gettingActualNumberInput = true;
							Screen.cursor();
							UpdateScreen( screenState );
						}
					}
					else
					{
						gettingNumberInput = true;
						UpdateScreen( screenState );
					}
				}
			}
			break;
		case KEY_STAR:
			if ( key.IsPressed() )
			{
				if ( screenState == SCREEN_OPTIONS )
				{
					if ( screenSubState == SCREEN_SUB_NONE )
					{
						UpdateScreen( SCREEN_DEF );
					}
					else if ( gettingNumberInput || gettingActualNumberInput )
					{
						gettingNumberInput = false;
						numberBuffer[ 0 ] = '\0';
						numberBufferPos = 0;
						gettingActualNumberInput = false;
						Screen.noCursor();
						UpdateScreen( screenState );
					}
					else
					{
						UpdateSubScreen( SCREEN_SUB_NONE, menuMaxMain );
					}
				}
			}
			break;
	}
}

void UpdateSensors()
{
	float_t temperature = 0.0f;
	float_t humidity = 0.0f;
	float_t temperature0 = 0.0f;
	float_t temperature1 = 0.0f;
	SensorsStatus status = Sensors.Read( temperature, humidity, temperature0, temperature1 );

	Serial.print( "Sensors: " );
	Serial.print( temperature );
	Serial.print( ' ' );
	Serial.print( humidity );
	Serial.print( ' ' );
	Serial.print( temperature0 );
	Serial.print( ' ' );
	Serial.print( temperature1 );

	if ( status.IsOK() )
	{
		State.Temperature = temperature;
		State.Humidity = humidity;
		State.Temperature0 = temperature0;
		State.Temperature1 = temperature1;

		if ( ( now() - lastUpdateOfHistory ) > Parameters.Update.History )
		{
			History.Write( State );
			lastUpdateOfHistory = now();
		}

		Serial.print( " OK" );
	}
	else
	{
		Serial.print( " ERROR" );
	}

	Serial.println();

	// Check the humidity limits.
	if ( State.Humidity < Parameters.Humidity.Low )
		Alarms.LowHumidity = true;
	else if ( State.Humidity > Parameters.Humidity.High )
		Alarms.HighHumidity = true;
	else
	{
		Alarms.LowHumidity = false;
		Alarms.HighHumidity = false;
	}

	// Check the temperature limits.
	if ( State.Temperature0 < Parameters.Temperature.Low0 )
		Alarms.LowTemperature0 = true;
	else if ( State.Temperature0 > Parameters.Temperature.High0 )
		Alarms.HighTemperature0 = true;
	else
	{
		Alarms.LowTemperature0 = false;
		Alarms.HighTemperature0 = false;
	}
	if ( State.Temperature1 < Parameters.Temperature.Low1 )
		Alarms.LowTemperature1 = true;
	else if ( State.Temperature1 > Parameters.Temperature.High1 )
		Alarms.HighTemperature1 = true;
	else
	{
		Alarms.LowTemperature1 = false;
		Alarms.HighTemperature1 = false;
	}
}

bool_t IsItDaytime()
{
	time_t daySeconds = ( hour() * SECONDS_PER_HOUR ) + ( minute() * SECONDS_PER_MINUTE ) + ( second() );

	return ( daySeconds >= Parameters.Lights.StartOfDay && daySeconds < Parameters.Lights.EndOfDay );
}

void Sunrise()
{
	State.Day = true;
	digitalWrite( PinOfLight, ValueOfLightOn );
}

void Sunset()
{
	State.Day = false;
	digitalWrite( PinOfLight, ValueOfLightOff );
}

void StartRaining()
{
	State.Raining = true;
	State.LastRain = now();
	digitalWrite( PinOfPump, ValueOfPumpOn );
}

void StopRaining()
{
	State.Raining = false;
	State.LastRain = now();
	digitalWrite( PinOfPump, ValueOfPumpOff );
}

void OnClientConnected()
{
	hasClient = true;
	UpdateScreen();
}
void OnClientDisconnected()
{
	hasClient = false;
	UpdateScreen();
}

bool_t OnSetTime( time_t t )
{
	// Clear the history to avoid wacky date differences...
	lastUpdateOfHistory = 0;
	History.Clear();
	// ...and set the time.
	setTime( t );
	return true;
}

bool_t OnSetPump( const PumpParams& params )
{
	// Sanity check the parameters.
	if ( params.ActiveFor == 0 || params.ActiveFor > SECONDS_PER_HOUR || params.ActivatedWait > SECONDS_PER_HOUR )
		return false;

	Parameters.Pump = params;
	return true;
}

bool_t OnSetTemperatureParameters( const TemperatureParams& params )
{
	// Sanity check the parameters.
	if ( params.Low0 > 80 ||
		 params.Low1 > 80 ||
		params.High0 > 80 ||
		params.High1 > 80 ||
		 params.Low0 >= params.High0 ||
		 params.Low1 >= params.High1 )
		return false;

	Parameters.Temperature = params;
	return true;
}

bool_t OnSetHumidityParameters( const HumidityParams& params )
{
	// Sanity check the parameters.
	if ( params.Low > 100 ||
		params.High > 100 ||
		 params.Low >= params.High )
		return false;

	Parameters.Humidity = params;
	return true;
}

bool_t OnSetDaytime( const LightParams& params )
{
	// Sanity check the parameters.
	if ( params.StartOfDay >= ( 24UL * SECONDS_PER_HOUR ) ||
		   params.EndOfDay >= ( 24UL * SECONDS_PER_HOUR ) ||
		 params.StartOfDay >= params.EndOfDay )
		return false;

	Parameters.Lights = params;
	return true;
}

bool_t OnSetUpdateParams( const UpdateParams& params )
{
	// Sanity check the parameters.
	if ( params.History > SECONDS_PER_HOUR ||
		 params.Sensors > SECONDS_PER_HOUR )
		return false;

	Parameters.Update = params;
	return true;
}

#ifndef TERRARIUM_ON_ARDUINO
int main()
{
	setup();

	while ( true )
	{
		loop();
	}

	return 0;
}
#endif
